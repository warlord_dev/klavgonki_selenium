import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import pages.GamePage;
import pages.HomePage;

public class Main {

    public static void main(String[] args) {
        System.setProperty("file.encoding","UTF-8");
        WebDriver driver = new ChromeDriver();
        new HomePage(driver).beginGame();
        new GamePage(driver).playGame();
    }
}
