package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;



import static java.util.concurrent.TimeUnit.SECONDS;

public class HomePage {

    private final WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        driver.get("http://klavogonki.ru/");
    }

    public void beginGame() {
        driver.findElement(By.xpath("//*[@id=\"index\"]/div[1]/div[2]/div/a[1]/img")).click();
        driver.findElement(By.xpath("//*[@id=\"howtoplay\"]/div[2]/div/table/tbody/tr[2]/td[2]/p[5]/input")).click();
    }
}
