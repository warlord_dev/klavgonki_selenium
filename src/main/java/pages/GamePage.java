package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class GamePage {
    private final WebDriver driver;

    public GamePage(WebDriver driver) {
        this.driver = driver;
        System.out.println("GamePAge");
    }

    public void playGame() {
        WebDriverWait wait = new WebDriverWait(driver, 40);
        WebElement newgame = driver.findElement(By.id("host_start"));
        if (newgame.isDisplayed()) {
            newgame.sendKeys(Keys.chord(Keys.CONTROL, Keys.ENTER));
        }
        By beforefocus = By.xpath("//*[@id=\"beforefocus\"]");
        wait.until(presenceOfElementLocated(beforefocus));
        WebElement input = driver.findElement(By.id("inputtext"));
        if (!input.isSelected()) {
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        }
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String game = driver.findElement(By.cssSelector("#typetext")).getText();
        System.out.println(game);
        String[] words = game.split(" ");
        for (String word : words) {
            input.clear();
            input.click();
                char [] myCharArray = word.toCharArray();
                for (char a : myCharArray ){
                  String b = "" + a;
                    try {
                        Random random = new Random();
                        random.setSeed(900);
                        Thread.sleep(random.nextInt(1300));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                  input.sendKeys(b);
                    try {
                        Thread.sleep(new Random().nextInt(800));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                  System.out.println(b);
                }
                input.sendKeys(Keys.SPACE);
            System.out.println(word);
        }
    }
}
